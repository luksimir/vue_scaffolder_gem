# frozen_string_literal: true
module ScaffolderGem
  module Generators
    class InstallGenerator < Rails::Generators::Base
      desc "Copy files to lib"
      source_root File.expand_path('../templates', __FILE__)

      def info_bootstrap
        #puts "IMA"
      end

      def copy_config
        #puts "IMA"
      end

      def copy_scaffold_template
        puts "Copy view templates"
        copy_file "_New.vue", "lib/templates/erb/scaffold/_New.vue"
        copy_file "New.vue", "lib/templates/erb/scaffold/New.vue"
        copy_file "Edit.vue", "lib/templates/erb/scaffold/Edit.vue"
        copy_file "Form.vue", "lib/templates/erb/scaffold/Form.vue"
        copy_file "Index.vue", "lib/templates/erb/scaffold/Index.vue"

        puts "Copy model template"
        copy_file "model.rb", "lib/templates/active_record/model/model.rb"

        puts "Copy controller template"
        copy_file "controller.rb", "lib/templates/rails/scaffold_controller/controller.rb"

        puts "Copy scaffold generator"
        copy_file "scaffold_generator.rb", "lib/rails/generators/erb/scaffold/scaffold_generator.rb"

      end

      def show_readme
        readme "README"
      end
    end
  end
end
