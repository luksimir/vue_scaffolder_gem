<% # noinspection RubyInstanceVariableToStringInspection
    if namespaced? %>
require_dependency "<%= namespaced_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController
  before_action :set_<%= singular_table_name %>, only: [:show, :edit, :update, :destroy]

  # GET <%= route_url %>
  def index
    @<%= plural_table_name %> = <%= orm_class.all(class_name) %>
    @filterrific = initialize_filterrific(
        @<%= plural_table_name %>,
        params[:filterrific],
        default_filter_params: { with_trashed: 'active' },
    ) or return
    @<%= plural_table_name %> = @filterrific.find
    pagy, paged_<%= plural_table_name %> = pagy(
        @<%= plural_table_name %>
    )

    response = {inertia: '<%= controller_class_name %>/Index',
                props: {
                    <%= plural_table_name %>: jbuilder do |json|
                      json.data(paged_<%= plural_table_name %>) do |<%= singular_table_name %>|
                        json.(<%= singular_table_name %>, :id, :title, :deleted_at)
                      end
                      json.meta pagy_metadata(pagy)
                    end,
                    filters: @filterrific.to_hash
    }}

    render response


  end

  # GET <%= route_url %>/1
  def show
    @<%= singular_table_name %> = <%= class_name %>.find(params[:id])
    render inertia: '<%= controller_class_name %>/Edit', props: {
        <%= singular_table_name %>: jbuilder do |json|
          json.(@<%= singular_table_name %>, :id, :title, :deleted_at)
        end,
    }
  end


  def new
    render inertia: '<%= controller_class_name %>/New', props: {
    }
  end


  # GET <%= route_url %>/1/edit
  def edit
    @<%= singular_table_name %> = <%= class_name %>.find(params[:id])
    render inertia: '<%= controller_class_name %>/Edit', props: {
        <%= singular_table_name %>: jbuilder do |json|
          json.(@<%= singular_table_name %>, :id, :title, :deleted_at)
        end,
    }
  end

  # POST <%= route_url %>
  def create
    @<%= singular_table_name %> = <%= class_name %>.new
    if @<%= singular_table_name %>.update(<%= singular_table_name %>_params)
      redirect_to <%= plural_table_name %>_path, notice: '<%= class_name %> created.'
    else
      redirect_to <%= plural_table_name %>_path, errors: @<%= singular_table_name %>.errors
    end
  end

  # PATCH/PUT <%= route_url %>/1
  def update
    if @<%= singular_table_name %>.update(<%= singular_table_name %>_params)
      redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), notice: '<%= class_name %> updated.'
    else
      redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), errors: @<%= singular_table_name %>.errors
    end
  end


  def destroy
    if @<%= singular_table_name %>.soft_delete
      if can? :edit, @<%= singular_table_name %>
        redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), notice: '<%= class_name %> deleted.'
      else
        redirect_to <%= plural_table_name %>_path, notice: '<%= class_name %> deleted.'
      end
    else
      redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), alert: '<%= class_name %> cannot be deleted!'
    end
  end

  def restore
    if @<%= singular_table_name %>.restore
      redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), notice: '<%= class_name %> restored.'
    else
      redirect_to edit_<%= singular_table_name %>_path(@<%= singular_table_name %>), alert: '<%= class_name %> cannot be restored!'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_<%= singular_table_name %>
        @<%= singular_table_name %> = <%= orm_class.find(class_name, "params[:id]") %>
    end

    # Only allow a trusted parameter "white list" through.
    def <%= "#{singular_table_name}_params" %>
    params.fetch(:<%= singular_table_name %>, {}).permit(
        :title
    )
    end
end
<% end -%>
