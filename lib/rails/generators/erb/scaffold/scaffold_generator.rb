# frozen_string_literal: true

require "rails/generators/erb"
require "rails/generators/resource_helpers"

module Erb # :nodoc:
  module Generators # :nodoc:
    class ScaffoldGenerator < Base # :nodoc:
      include Rails::Generators::ResourceHelpers

      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      def create_root_folder
        #empty_directory File.join("app/views", controller_file_path)
        empty_directory File.join("app/javascript/Pages/#{plural_table_name.titleize}")
      end

      def copy_view_files
        # available_views.each do |view|
        #   formats.each do |format|
        #     filename = filename_with_extensions(view, format)
        #     template filename, File.join("app/javascript/Pages", plural_table_name.titleize, filename)
        #   end
        # end
        template "_New.vue", File.join("app/javascript/Pages/#{plural_table_name.titleize}", "_New.vue")
        template "New.vue", File.join("app/javascript/Pages/#{plural_table_name.titleize}", "New.vue")
        template "Edit.vue", File.join("app/javascript/Pages/#{plural_table_name.titleize}", "Edit.vue")
        template "Form.vue", File.join("app/javascript/Pages/#{plural_table_name.titleize}", "Form.vue")
        template "Index.vue", File.join("app/javascript/Pages/#{plural_table_name.titleize}", "Index.vue")
      end

      private

        # def available_views
        #   %w(Index.vue Form.vue _New.vue, Edit.vue)
        # end
    end
  end
end
