Gem::Specification.new do |s|
  s.name = %q{vue_scaffolder_gem}
  s.version = "1.6.0"
  s.date = %q{2020-02-27}
  s.summary = %q{scaffolder for VUEAdmin}
  s.description = "A gem that overrides the default scaffolder, so the views and controllers match the ones used in Vue"
  s.authors = %q{Luka Bizant}
  s.email = 'luka@prelom.digital'
  s.homepage = 'https://prelom.digital'
  s.files = [
    "lib/rails/generators/erb/scaffold/scaffold_generator.rb",
    "lib/templates/active_record/model/model.rb",
    "lib/templates/rails/scaffold_controller/controller.rb"
  ]
  s.require_paths = ["lib", "lib/templates", "lib/generators"]
  s.license       = 'MIT'
end