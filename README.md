# README

- Install by running:

`scaffolder_gem:install`

- To generate a scaffold you need to include "title" and "deleted_at" attributes like this:

`rails g scaffold [ModelName] title:string deleted_at:datetime attribute:type attribute2:type ...`